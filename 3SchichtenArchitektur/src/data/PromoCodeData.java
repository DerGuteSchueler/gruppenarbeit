package data;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class PromoCodeData implements IPromoCodeData {

	private List<String> promocodes;
	
	
	
	public PromoCodeData(){
		this.promocodes = new LinkedList<String>();
		PrintWriter pWriter = null;
	    try { 
	        pWriter = new PrintWriter(new BufferedWriter(new FileWriter("save.txt"))); 
			pWriter.println(this.promocodes); 
	    } catch (IOException ioe) { 
	        ioe.printStackTrace(); 
	    } finally { 
	        if (pWriter != null){ 
	            pWriter.flush(); 
	            pWriter.close(); 
	        }
	    }
	}
	
	@Override
	public boolean savePromoCode(String code) {
		return this.promocodes.add(code);
	}

	@Override
	public boolean isPromoCode(String code) {
		return this.promocodes.contains(code);
	}

}
